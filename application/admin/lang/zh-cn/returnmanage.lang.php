<?php
$lang['return_order_returnsn'] = '退货编号';
$lang['return_buyer_message'] = '退货原因';

$lang['wechat_tm_order_refund'] = '退款审核通知';
$lang['order_refund_agree'] = '您的退款申请已同意';
$lang['order_refund_reject'] = '您的退款申请已拒绝';
return $lang;
?>
